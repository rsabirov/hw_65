import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://burger-template-3b726.firebaseio.com/' // Your URL here!
});

export default instance;

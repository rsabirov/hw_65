import React, {Fragment} from 'react';
import Toolbar from "../Navigation/Toolbar/Toolbar";
import './layout.css';

const Layout = props => (
  <Fragment>
    <Toolbar />
    <main className="layout-content">
      {props.children}
    </main>
  </Fragment>
);

export default Layout;
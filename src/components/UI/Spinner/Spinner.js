import React from 'react';
import './spinner.css';

const Spinner = () =>(
  <div className="Spinner">Loading...</div>
);

export default Spinner;
import React from 'react';
import './logo.css';
import burgerLogo from '../../../assets/images/burger_logo.png';

const Logo = () => (
  <div className="Logo">
    <img src={burgerLogo} alt="MyBurger"/>
  </div>
);

export default Logo;
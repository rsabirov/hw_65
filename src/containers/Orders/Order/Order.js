import React from 'react';
import './order.css';

const OrderItem = props => {

  const ingredients = [];

  for (let ingredientName in props.ingredients) {
    ingredients.push({
      name: ingredientName,
      amount: props.ingredients[ingredientName]
    })
  }

  const ingredientsOutput = ingredients.map(ig => (
    <span key={ig.name}>{ig.name} ({ig.amount})</span>
  ));

  return (
    <div className="OrderItem">
      <p>Ingredients: {ingredientsOutput}</p>
      <p>Price: <strong>{props.price} KGS</strong></p>
    </div>
  );
};

export default OrderItem;